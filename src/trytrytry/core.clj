(ns trytrytry.core
  (:require
   [clj-http.lite.client :as http]
   [clojure.edn :as edn]
   [resilience4clj-retry.core :as r])
  (:gen-class))

(set! *warn-on-reflection* true)

(defn- try-connect [url]
  (System/setProperty "java.library.path"
                      (str (System/getenv "GRAALVM_HOME") "/jre/lib"))
  (http/get url))


(defn log-retries! [retry]
  (r/listen-event retry
                  :RETRY
                  (fn [evt]
                    (println (str "Received event " (:event-type evt))))))



(defn ^#:inclined{:option.url!      {:desc "(required) URL to try"}
                  :option.max-tries {:desc "Maximum tries" :default "3"}
                  :option.wait      {:desc "Time between tries (ms)" :default "1000"}}
  -main [{:keys [url max-tries wait]}]
  (let [retry         (r/create "connect" {:max-attempts  (edn/read-string max-tries)
                                           :wait-duration (edn/read-string wait)})
        retry-connect (r/decorate try-connect retry)
        _             (log-retries! retry)]
    (when-not (retry-connect url)
      (System/exit 1))))
