(ns core
  (:require
   [inclined.main :as inclined]
   [trytrytry.core])
  (:gen-class))

(set! *warn-on-reflection* true)

(defn -main [& args]
  (let [inclined-args (concat '("--ns" "trytrytry.core" "--") args)]
    (apply inclined/-main inclined-args)))
